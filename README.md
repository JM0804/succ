# succ.wiki

- A wiki for all things succulent and cacti, built using [Lektor](https://www.getlektor.com/)
- Hosted by Netlify and available at https://succ.wiki/

[![Netlify Status](https://api.netlify.com/api/v1/badges/73857d80-46d4-42b4-957c-698a205e4a27/deploy-status)](https://app.netlify.com/sites/succ-wiki/deploys)

## Requirements

- `python3`
- `pip3`

### pip requirements

- Run `pip install -r requirements.txt`

## Virtual environment

It is recommended that you use a virtual environment before installing any packages to avoid any conflicts with your system and other programs. To set this up, do the following:

- Install `python3-venv` globally
- Run `python3 -m venv venv` in the project root directory
- Run `source venv/bin/activate` in the project root directory
- Now install the pip requirements

If you use a virtual environment you will need to run `source venv/bin/activate` at the start of each terminal session in order to enter the virtual environment and run the packages installed within it.

## Colour scheme

URL: http://paletton.com/#uid=a2l590kllll00LZaGtvw0db++50

**Primary colour**

| | hex | rgb | rgba | rgb0 
|-|-|-|-|-|
| shade 0 | `#7D9F35` | `rgb(125,159,53)` | `rgba(125,159,53,1)` | `rgb0(0.49,0.624,0.208)` |
| shade 1 | `#F7F7F7` | `rgb(247,247,247)` | `rgba(247,247,247,1)` | `rgb0(0.969,0.969,0.969)` |
| shade 2 | `#C4DC93` | `rgb(196,220,147)` | `rgba(196,220,147,1)` | `rgb0(0.769,0.863,0.576)` |
| shade 3 | `#436200` | `rgb(67,98,0)` | `rgba(67,98,0,1)` | `rgb0(0.263,0.384,0)` |
| shade 4 | `#192500` | `rgb(25,37,0)` | `rgba(25,37,0,1)` | `rgb0(0.098,0.145,0)` |

**Complementary colour**

| | hex | rgb | rgba | rgb0 |
|-|-|-|-|-|
| shade 0 | `#872D62` | `rgb(135,45,98)` | `rgba(135,45,98,1)` | `rgb0(0.529,0.176,0.384)` |
| shade 1 | `#E6E6E6` | `rgb(230,230,230)` | `rgba(230,230,230,1)` | `rgb0(0.902,0.902,0.902)` |
| shade 2 | `#BB7DA1` | `rgb(187,125,161)` | `rgba(187,125,161,1)` | `rgb0(0.733,0.49,0.631)` |
| shade 3 | `#530031` | `rgb(83,0,49)` | `rgba(83,0,49,1)` | `rgb0(0.325,0,0.192)` |
| shade 4 | `#200013` | `rgb(32,0,19)` | `rgba(32,0,19,1)` | `rgb0(0.125,0,0.075)` |
