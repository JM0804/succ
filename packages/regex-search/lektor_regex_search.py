# -*- coding: utf-8 -*-
from lektor.pluginsystem import Plugin
import re

def re_search(regex, text):
	search = re.search(regex, text)
	if search is not None:
		return search.group(1)

class RegexSearchPlugin(Plugin):
	name = 'Regex Search'
	description = u'Adds a global function that enables re.search'

	def on_process_template_context(self, context, **extra):
		def test_function():
			return 'Value from plugin %s' % self.name
		context['test_function'] = test_function

	def on_setup_env(self, **extra):
		self.env.jinja_env.globals.update(
			re_search=re_search
		)
