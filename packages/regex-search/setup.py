from setuptools import setup

setup(
    name='lektor-regex-search',
    version='0.1',
    author='Jonathan Mason',
    py_modules=['lektor_regex_search'],
    entry_points={
        'lektor.plugins': [
            'regex-search = lektor_regex_search:RegexSearchPlugin',
        ]
    }
)
